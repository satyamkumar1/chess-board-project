
const mainbox = document.querySelector('.main-container');


const Size = 8;
let row;
let col;

for (let i = 1; i <= Size; i++) {

    row = document.createElement('div');
    row.classList.add('row')

    for (let j = 1; j <= Size; j++) {
        col = document.createElement('div');
        col.dataset.j = j;
        col.dataset.i = i;

        col.classList.add('col');
        row.appendChild(col);

    }
    mainbox.appendChild(row);


    console.log(row);

}

// console.log(col);

mainbox.addEventListener('click', (event) => {

    event.target.style.backgroundColor = "red";
    console.log(event.target.dataset);
    const rowind = event.target.dataset.i;
    const column = event.target.dataset.j;


    for (let i = 1; i <= Size; i++) {

        for (let j = 1; j <= Size; j++) {


            if (Math.abs(i - rowind) == Math.abs(j - column)) {
                document.querySelector(`[data-i="${i}"][data-j="${j}"]`).style.backgroundColor = "red";
            }

        }

    }



})
